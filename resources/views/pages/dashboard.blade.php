<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/mdb/css/mdb.min.css">
    <!-- Font Awesome -->
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
    />
    <title>Dashboard</title>
</head>
<body>
<div id="app">
    <div class="container">
        <div class="card mt-5 p-4">
            <div class="row">
                <div class="col-11">
                    <div class="back-link d-inline">
                        <a href="{{route('home')}}"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
                <div class="col-1">
                    <div class="logout-link d-inline">
                        <a href="{{route('logout')}}">Logout <i class="fa fa-arrow-right-from-bracket"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <user-profile-component :user="{{Auth::user()}}" :role="{{Auth::user()->getRoleNames()}}"></user-profile-component>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <settings-profile-component></settings-profile-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{mix('js/app.js')}}"></script>
<script src="/assets/mdb/js/mdb.min.js"></script>
</body>
</html>
