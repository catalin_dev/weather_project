<div class="card shadow-5 mt-5">
    <div class="card-body">
        <h3 class="card-title">
           Login
        </h3>
        <form action="{{route('login.post')}}" method="post" class="formLogin">
            @csrf
            <div class="form-outline w-100 mt-5">
                <input type="text" name="email" class="form-control">
                <label for="" class="form-label">Email</label>
            </div>
            <div class="form-outline w-100 mt-4">
                <input type="password" name="password" class="form-control">
                <label for="" class="form-label">Password</label>
            </div>
            <div class="form-check mt-2">
                <input type="checkbox" class="form-check-input" name="remember" id="rememberField">
                <label for="rememberField" class="form-check-label">Remember me</label>
            </div>
            <button class="btn btn-primary mt-2 w-100 mb-4" type="submit">Login</button>
        </form>
        <span class="text-muted mt-5">You haven't account ? <a data-mdb-toggle="modal" href="#modalCreateAccount" role="button">Create Account</a></span>

    </div>
</div>

<!-- First modal dialog -->
<div class="modal fade" id="modalCreateAccount" aria-hidden="true" aria-labelledby="modalCreateAccountToggleLabel" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCreateAccountToggleLabel">Register</h5>
                <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
