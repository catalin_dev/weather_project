<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/mdb/css/mdb.min.css">
    <!-- Font Awesome -->
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
    />
    <title>@yield('title')</title>
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-8 col-sm-12">
                <main>
                    <div class="card mt-5">
                        <div class="card-body">

                            @yield('content')
                        </div>
                    </div>
                </main>
            </div>
            <div class="col-md-4 col-sm-12">
                <aside>
                    @guest
                        @include('auth.auth')
                    @endguest
                    @auth
                        <div class="card mt-5">
                            <div class="card-body">
                                <p>
                                    <b>Hello, </b> {{auth()->user()->name}}
                                </p>
                                <a href="{{route('dashboard')}}">Dashboard</a>
                                <a href="{{route('logout')}}">Logout</a>
                            </div>
                        </div>
                    @endauth
                </aside>
            </div>
        </div>
    </div>

    <script src="{{mix('js/app.js')}}"></script>
    <script src="/assets/mdb/js/mdb.min.js"></script>
</body>
</html>
